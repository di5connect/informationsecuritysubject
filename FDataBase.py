import math
import sqlite3
import time


class FDataBase:
    def __init__(self, db):
        self.__db = db
        self.__cur = db.cursor()

    def if_username_free(self, username):
        self.__cur.execute(f"SELECT COUNT() as 'count' FROM users WHERE username LIKE '{username}'")
        res = self.__cur.fetchone()
        return not res['count'] > 0

    def addUser(self, username, hpsw, roll):
        error = ""
        try:
            if not self.if_username_free(username):
                return False

            tm = math.floor(time.time())
            self.__cur.execute("INSERT INTO users VALUES(NULL, ?, ?, ?, ?)", (username, hpsw, roll, tm))
            self.__db.commit()
        except sqlite3.Error as e:
            return False

        return True

    def del_user(self, user_id):
        try:
            self.__cur.execute(f"DELETE FROM users WHERE id = '{user_id}'")
            self.__db.commit()
        except sqlite3.Error as e:
            return False

        return True

    def getUser(self, user_id):
        try:
            self.__cur.execute(f"SELECT * FROM users WHERE id = {user_id} LIMIT 1")
            res = self.__cur.fetchone()
            if not res:
                # Пользователь не найден
                return None

            return res
        except sqlite3.Error as e:
            return None

    def get_all_users(self):
        self.__cur.execute("SELECT id, username FROM users WHERE id != 0")
        return self.__cur.fetchall()

    def getUserByUserName(self, username):
        try:
            self.__cur.execute(f"SELECT * FROM users WHERE username = '{username}' LIMIT 1")
            res = self.__cur.fetchone()
            if not res:
                # Пользователь не найден
                return False

            return res
        except sqlite3.Error as e:
            return False

    def change_user_password(self, user_name, new_password):
        try:
            self.__cur.execute(f"UPDATE users SET password = ? WHERE username = ?", (new_password, user_name))
            self.__db.commit()

        except sqlite3.Error as e:
            print("Ошибка смены пароля:" + str(e))
            return False
        return True

    def change_username(self, userid, new_username):
        try:
            self.__cur.execute(f"UPDATE users SET username = ? WHERE id = ?", (new_username, userid))
            self.__db.commit()

        except sqlite3.Error as e:
            print("Ошибка смены пароля:" + str(e))
            return False
        return True
