import sqlite3
import os
from flask import Flask, render_template, request, g, flash, redirect, url_for
from flask_login import LoginManager, login_user, login_required, logout_user, current_user
from UserLogin import UserLogin

from PIL import Image

from FDataBase import FDataBase

from werkzeug.security import generate_password_hash, check_password_hash

# конфигурация бд
DATABASE = "/tmp/data_base/di5BD.db"
DEBUG = True
SECRET_KEY = "700c9f3f5543b4e8d5df3e17e86cab15dd9312f1"
dbase = None  # переменная для соединения с бд

app = Flask(__name__)  # создаем объект Flask
app.config.from_object(__name__)  # берем конфигурацию из нынешнего файла
app.config.update(dict(DATABASE=os.path.join(app.root_path, "data_base/di5BD.db")))

login_manager = LoginManager(app)  # управление процессом авторизации для нашего приложения
login_manager.login_view = 'login'
login_manager.login_message = "Авторизуйтесь для доступа к данной странице"
login_manager.login_message_category = "success"


@login_manager.user_loader
def load_user(user_id):
    return UserLogin().fromDB(user_id, dbase)


def create_db():
    """Вспомогательная функция для создания таблиц БД"""
    db = connect_db()
    # sq_db.sql - отдельный файл, в котором прописаны sql запросы для создания таблиц
    with app.open_resource("./data_base/sq_db.sql", mode='r') as f:
        db.cursor().executescript(f.read())  # запуск sql-скриптов из файла
    db.commit()
    db.close()


def connect_db():
    """Общая функция для установления соединения с БД"""
    conn = sqlite3.connect(app.config["DATABASE"])  # берем бд из конфигурации приложения
    conn.row_factory = sqlite3.Row  # чтобы записи из бд были представлены в виде словаря
    return conn


def get_db():
    """Соединение с БД, если оно еще не установленно"""
    if not hasattr(g, 'link_db'):
        g.link_db = connect_db()
    return g.link_db


@app.teardown_appcontext
def close_db(error):
    """Закрываем соединение с БД, если оно было установленно"""
    if hasattr(g, 'link_db'):
        g.link_db.close()


@app.route('/admin', methods=["GET", "POST"])
@login_required  # Декоратор для администратора
def admin_page():
    if current_user.get_username() == "root" and current_user.get_id() == "0":
        if request.method == "POST":
            if request.form['action'] == 'Добавить':
                for i in request.form:
                    dbase.addUser(request.form['username'],
                                  generate_password_hash(request.form['password']),
                                  request.form['role'])
            else:
                user = UserLogin().fromDB(request.form["user_id"], dbase)
                if user:
                    if request.form['action'] == 'Удалить':
                        dbase.del_user(user.get_id())
                    elif request.form['action'] == 'Сохранить':
                        if len(request.form["new_password"]) > 0:
                            hash = generate_password_hash(request.form["new_password"])
                            dbase.change_user_password(user.get_username(), hash)
                        if not user.get_username() == request.form["new_username"]:
                            dbase.change_username(request.form["user_id"], request.form["new_username"])

        # Список всех пользователей из базы данных
        users = dbase.get_all_users()
        return render_template('admin.html', users=users)
    else:
        return render_template('you_dont_admin.html')


@app.route("/")
def index():
    return render_template('info_page.html')


@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash("Вы вышли из аккаунта", "success")
    return redirect(url_for('login'))


@app.route('/delete_profile', methods=["GET", "POST"])
@login_required  # Предположим, что вы используете декоратор для аутентификации
def delete_profile():
    if request.method == "POST":
        res = dbase.del_user(current_user.get_id())
        if res:
            logout_user()
            # После успешного удаления профиля, отправьте flash-сообщение
            flash("Ваш профиль был успешно удален", "success")

            # Затем перенаправьте пользователя на страницу авторизации (или другую страницу на ваш выбор)
            return redirect(url_for('login'))
        else:
            flash("Ошибка удаления профиля", "error")
    else:
        # logout_user()
        # flash("Ваш профиль был успешно удален", "success")
        return redirect(url_for('profile'))


@app.route('/profile', methods=["POST", "GET"])
@login_required
def profile():
    if current_user.get_username() == "root" and current_user.get_id() == "0":
        return redirect(url_for('admin_page'))
    if request.method == "POST":
        if current_user and check_password_hash(current_user.get_password_hash(),
                                                request.form['current_password']):
            if check_password_requirements(request.form['new_password']):
                # если совпал актуальный пароль, делаем хеш нового:
                hash = generate_password_hash(request.form['new_password'])
                # делаем запрос в бд для смены пароля:
                res = dbase.change_user_password(current_user.get_username(), hash)
                if res:
                    flash("Пароль был успешно изменен!", "success")
                else:
                    flash("Не удалось поменять пароль!", "error")
            else:
                flash("Введенный пароль не соответствует требованиям: длина не меньше 4х "
                      "символов!", "error")
        else:
            flash("Неверный пароль пользователя!", "error")

    return render_template('person_page.html', username=current_user.get_username())


@app.route("/login", methods=["POST", "GET"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('profile'))

    if request.method == "POST":
        user = dbase.getUserByUserName(request.form['username'])
        if user and check_password_hash(user['password'], request.form['password']):
            userlogin = UserLogin().create(user)
            rm = True if request.form.get('remember-me') else False
            login_user(userlogin, remember=rm)
            return redirect(url_for('profile'))

        flash("Неверные логин или пароль", "error")

    return render_template('login.html')


def caesar_cipher(text, shift, decrypt=False):
    result = ""
    direction = -1 if decrypt else 1
    for char in text:
        if char.isalpha():
            ascii_offset = ord('А') if char.isupper() else ord('а')
            result += chr((ord(char) - ascii_offset + direction * shift) % 32 + ascii_offset)
        else:
            result += char
    return result


def encrypt_file(file_path, shift):
    with open(file_path, 'r', encoding='utf-8') as file:
        content = file.read()
    encrypted_content = caesar_cipher(content, shift)
    encrypted_file_path = f"{file_path}.encrypted"
    with open(encrypted_file_path, 'w', encoding='utf-8') as encrypted_file:
        encrypted_file.write(encrypted_content)
    return encrypted_file_path


def decrypt_file(file_path, shift):
    with open(file_path, 'r', encoding='utf-8') as file:
        content = file.read()
    decrypted_content = caesar_cipher(content, shift, decrypt=True)
    decrypted_file_path = f"{os.path.splitext(file_path)[0]}_decrypted.txt"
    with open(decrypted_file_path, 'w', encoding='utf-8') as decrypted_file:
        decrypted_file.write(decrypted_content)
    return decrypted_file_path


@app.route('/encrypt_file', methods=['POST'])
def encrypt_file_route():
    shift = int(request.form['shift'])
    file = request.files['file']

    if file:
        file_path = f"uploads/{file.filename}"
        file.save(file_path)
        encrypted_file_path = encrypt_file(file_path, shift)
        os.remove(file_path)
        return redirect(url_for('encrypt', encrypted_file_path=encrypted_file_path))

    return redirect(url_for('index'))


@app.route('/decrypt_file', methods=['POST'])
def decrypt_file_route():
    shift = int(request.form['shift'])
    file = request.files['file']

    if file:
        file_path = f"uploads/{file.filename}"
        file.save(file_path)
        decrypted_file_path = decrypt_file(file_path, shift)
        os.remove(file_path)
        return redirect(url_for('decrypt', decrypted_file_path=decrypted_file_path))

    return redirect(url_for('index'))


@app.route('/encrypt', methods=['GET', 'POST'])
def encrypt():
    encrypted_text = None
    if request.method == 'POST':
        text = request.form['text']
        shift = int(request.form['shift'])
        encrypted_text = caesar_cipher(text, shift)
    return render_template('encrypt.html', encrypted_text=encrypted_text)


@app.route('/decrypt', methods=['GET', 'POST'])
def decrypt():
    decrypted_text = None
    decrypted_file_path = None

    if request.method == 'POST':
        text = request.form['text']
        shift = int(request.form['shift'])
        decrypted_text = caesar_cipher(text, shift, decrypt=True)

    return render_template('decrypt.html', decrypted_text=decrypted_text, decrypted_file_path=decrypted_file_path)


def hide_text_in_image(image_path, text):
    # Загрузка изображения
    img = Image.open(image_path)
    pixels = list(img.getdata())

    # Преобразование текста в двоичную строку
    binary_text = ''.join(format(ord(char), '08b') for char in text)

    # Сокрытие текста в изображении (метод наименее значимого бита)
    pixel_index = 0
    for bit in binary_text:
        pixel = pixels[pixel_index]

        # Проверка, является ли pixel числом (если да, создаем кортеж)
        if not isinstance(pixel, tuple):
            pixel = (pixel,)

        # Обновление последнего элемента в кортеже
        new_pixel = pixel[:-1] + (int(bit),)

        # Установка нового пикселя
        img.putpixel((pixel_index % img.width, pixel_index // img.width), new_pixel)

        pixel_index += 1

    # Создание нового изображения с сокрытым текстом
    hidden_image_path = f"uploads/{os.path.splitext(os.path.basename(image_path))[0]}_hidden.png"
    img.save(hidden_image_path)

    return hidden_image_path


def extract_text_from_image(hidden_image_path):
    # Загрузка изображения
    img = Image.open(hidden_image_path)
    pixels = list(img.getdata())

    # Извлечение текста из изображения (метод наименее значимого бита)
    extracted_text = ''
    current_byte = 0
    bit_count = 0

    for pixel in pixels:
        # Проверка, является ли pixel числом (если да, создаем кортеж)
        if not isinstance(pixel, tuple):
            pixel = (pixel,)

        # Извлечение младшего бита
        current_byte = (current_byte << 1) | (pixel[0] & 1)
        bit_count += 1

        # Каждый байт состоит из 8 бит, поэтому когда мы извлекли 8 бит, преобразуем его в символ
        if bit_count == 8:
            # Проверка на символ конца текста (null-байт)
            if current_byte == 0:
                break

            # Проверка на печатаемость ASCII-символа
            if 32 <= current_byte <= 126 or current_byte == 10:  # Исключаем символ конца строки
                extracted_text += chr(current_byte)

            current_byte = 0
            bit_count = 0

    return extracted_text


@app.route('/steganography')
def steganography():
    return render_template('steganography.html')


@app.route('/hide_text', methods=['POST'])
def hide_text():
    text = request.form['text']
    image = request.files['image']

    if image:
        image_path = f"uploads/{image.filename}"
        image.save(image_path)
        hidden_image_path = hide_text_in_image(image_path, text)
        os.remove(image_path)
        return redirect(url_for('steganography', hidden_image_path=hidden_image_path))

    return redirect(url_for('steganography'))


@app.route('/extract_text', methods=['POST'])
def extract_text():
    image = request.files['image']

    if image:
        image_path = f"uploads/{image.filename}"
        image.save(image_path)
        extracted_text = extract_text_from_image(image_path)
        return render_template('steganography.html', extracted_text=extracted_text)

    return redirect(url_for('steganography'))


def check_password_requirements(password):
    import re
    errors = []

    # Проверка на минимальную длину
    if len(password) < 8:
        errors.append("Пароль слишком короткий, минимальная длина 8 символов.")

    # Проверка на наличие заглавных букв
    if not any(char.isupper() for char in password):
        errors.append("Пароль должен содержать хотя бы одну заглавную букву.")

    # Проверка на наличие строчных букв
    if not any(char.islower() for char in password):
        errors.append("Пароль должен содержать хотя бы одну строчную букву.")

    # Проверка на наличие цифр
    if not any(char.isdigit() for char in password):
        errors.append("Пароль должен содержать хотя бы одну цифру.")

    # Проверка на наличие специальных символов
    if not re.search(r'[!@#$%^&*()_,.?":{}|<>]', password):
        errors.append("Пароль должен содержать хотя бы один специальный символ: !@#$%^&*()_,.?\":{}|<>")

    return "\n".join(errors)


def check_register_data():
    if len(request.form['username']) <= 3:
        flash("Слишком короткое имя пользователя!", "error")
        return False
    elif request.form['username'] == "root":
        flash("Некорректное имя пользователя!", "error")
        return False
    elif not dbase.if_username_free(request.form['username']):
        flash("Это имя пользователя уже занято! Обратитесь к администратору для организации аукциона", "error")
        return False

    errors = check_password_requirements(request.form['password'])
    if errors:
        flash(errors, "error")
    else:
        if request.form['password'] == request.form['confirm-password']:
            return True
    return False


@app.route("/register", methods=["POST", "GET"])
def register():
    if request.method == "POST":
        if check_register_data():
            hash = generate_password_hash(request.form['password'])
            res = dbase.addUser(request.form['username'], hash, 'user')
            if res:
                flash("Вы успешно зарегистрированы", "success")
                return redirect(url_for('login'))
            else:
                flash("Ошибка регистрации", "error")

    username = request.form['username'] if 'username' in request.form.keys() else ''
    return render_template('register.html', username=username)


@app.before_request
def before_request():
    """Установление соединения с БД перед выполнением запроса"""
    global dbase
    db = get_db()
    dbase = FDataBase(db)


if __name__ == '__main__':
    app.run()
