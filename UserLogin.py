class UserLogin:
    def fromDB(self, user_id, db):
        self.__user = db.getUser(user_id)
        return self

    def create(self, user):
        self.__user = user
        return self

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def is_admin(self):
        return self.__user['role'] == "root"

    def get_id(self):
        try:
            return str(self.__user['id'])
        except:
            return False

    def get_username(self):
        if self.__user:
            return str(self.__user['username'])

    def get_password_hash(self):
        if self.__user:
            return str(self.__user['password'])
